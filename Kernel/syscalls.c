#include <typeDefs.h>
#include <lib.h>
#include <video.h>
#include <audio.h>
#include <keyboard.h>	
#include <scheduler.h>
#include <vesa_video.h>

typedef int (*EntryPoint)();

int sys_read(FILE_DESC fd, char* string, size_t len){
	
	int bufferLength = waitBuffer(len);

	copyBuffer(string);
	//string[bufferLength] = 0;

	return bufferLength;

}

void sys_write(FILE_DESC fd, const char * string, size_t length){

	if(fd == STDERR)
		color_set(RED, BLACK);
	
	while (length--){
		print_char(*string++);
		update_written_space();
	}
	
	color_set_default();

}

void* sys_malloc(size_t size){
	return NULL;
}

/*
void sys_free(void * p){
	free(p);
}*/

void sys_cls(){
	clear_screen();
}

void sys_beep(){
	beep();
}

void sys_play_song(){
	playSong();
}

int sys_exit(int exit_code){
	return exit_code;
}

void sys_sleep(size_t s){

	sleeping = 1;
	sleepTimer = s;

	//_cli();
	
	while (sleeping){}
	
}

void sys_create_process(char * name, void * entry, uint8_t foreground){

	Process* new_proc = new_process(name, entry, foreground);
	enqueque_process(new_proc);

	//print_num_hex(((EntryPoint)entry)());

}

void sys_list_ps(){
	list_processes();
}

void sys_draw_test(){
	draw_random_circles();
}

void sys_kill_process(int pid) {
	kill(pid);
}