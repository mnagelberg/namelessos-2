/* 
 * extraido de BareMetal-OS
 * Orig. written by ohnx.
*/

#ifndef VESA_VIDEO_H
#define VESA_VIDEO_H

#include <lib.h>
#include <memmanager.h>
#include <stdint.h>


typedef struct {
	unsigned int x, y;
} point;

void vesa_set_video_mode();
void put_pixel(int x, int y, char red, char green, char blue);
void init_vesa_video();
void clear_vesa_screen();
void clear_buffer();
void flush_buffer();

void draw_line(unsigned int xs, unsigned int ys, unsigned int xd, unsigned int yd, unsigned int r, unsigned int g, unsigned int b);
void draw_frect(unsigned int x, unsigned int y, unsigned int l, unsigned int w, unsigned int r, unsigned int g, unsigned int b);
void draw_fcircle(unsigned int x, unsigned int y, int radius, unsigned int r, unsigned int g, unsigned int b);

void draw_schar(char to, unsigned int x, unsigned int y, unsigned int r, unsigned int g, unsigned int b);
void draw_char(char to, unsigned int x, unsigned int y, int size, unsigned int r, unsigned int g, unsigned int b);
void draw_text(char* text, int length, unsigned int x, unsigned int y, int size, unsigned int r, unsigned int g, unsigned int b);
void vesa_delete_char(unsigned int xd, unsigned int yd, int size);

void draw_random_circles();
void change_sys_color(unsigned int r, unsigned int g, unsigned int b);

#endif