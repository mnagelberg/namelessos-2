#ifndef TASK_H
#define TASK_H

task_t * create_task(const char * name, int argc, char ** argv);
void remove_task(task_t * task);

// tasks states
void task_get_ready(task_t * task);
void task_be_paused(task_t * task);
void task_start_sleeping(task_t * task, uint64_t ms);

#endif