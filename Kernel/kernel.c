#include <stdint.h>
#include <video.h>
#include <lib.h>
#include <moduleLoader.h>
#include <naiveConsole.h>
#include <audio.h>
#include <memmanager.h>
#include <vmm.h>
#include <vesa_video.h>
#include <scheduler.h>

#define PIT_FREQ 18

extern uint8_t text;
extern uint8_t rodata;
extern uint8_t data;
extern uint8_t bss;
extern uint8_t endOfKernelBinary;
extern uint8_t endOfKernel;

static void * const sampleCodeModuleAddress = (void*)0x400000;
static void * const sampleDataModuleAddress = (void*)0x500000;

static int pitTickCounter = 0;	

typedef int (*EntryPoint)();


void print_title(){
	print_string("\t\t _   _                      _                 _____ _____\n");
	print_string("\t\t| \\ | |                    | |               |  _  /  ___|\n");
	print_string("\t\t|  \\| | __ _ _ __ ___   ___| | ___  ___ ___  | | | \\ `--. \n");
	print_string("\t\t| . ` |/ _` | '_ ` _ \\ / _ \\ |/ _ \\/ __/ __| | | | |`--. \\\n");
	print_string("\t\t| |\\  | (_| | | | | | |  __/ |  __/\\__ \\__ \\ \\ \\_/ /\\__/ /\n");
	print_string("\t\t\\_| \\_/\\__,_|_| |_| |_|\\___|_|\\___||___/___/  \\___/\\____/\n");
}

void clearBSS(void * bssAddress, uint64_t bssSize)
{
	memset(bssAddress, 0, bssSize);
}

void * getStackBase()
{

	return (void*)(
		(uint64_t)&endOfKernel
		+ PAGE_SIZE * 8				//The size of the stack itself, 32KiB
		- sizeof(uint64_t)			//Begin at the top of the stack
	);
}

void pit_handler(){

	pitTickCounter++;

	if ( (pitTickCounter % (PIT_FREQ/10)) < 1 ){ //handle de dsegs
	
		if (sleepTimer > 0) {
			sleepTimer--;
		} else
			if (sleeping) {
				sleeping = 0;
				//_sti();
			}
	}		
}

void * initializeKernelBinary(){


	void * moduleAddresses[] = {
		sampleCodeModuleAddress,
		sampleDataModuleAddress
	};

	//print_string("Seteo los modulos\n\n");
	loadModules(&endOfKernelBinary, moduleAddresses);

	//print_string("Limpio la BSS\n\n");
	clearBSS(&bss, &endOfKernel - &bss);

	//print_string("Inicializo la memoria\n\n");
	initialize_memory();
	vmm_initialize();


	//print_string("Booteo...\n");
	return getStackBase();
}

int main(){	
	k_mutex(FALSE);

	clear_screen();	 
	print_title();
	print_line();
	print_line();

	audio_init();
	loadSong((char*)sampleDataModuleAddress);

	vesa_set_video_mode(); 

	Process* entry = new_process("Shell", sampleCodeModuleAddress, TRUE);
	enqueque_process(entry);

	k_mutex(TRUE);

	print_string("\t\t\t\t\t\tVersion 0.0.1\n");
	print_string("\t\t\t\tMartin Nagelberg - Jeremias Aagaard\n\n");
	print_num_hex(((EntryPoint)sampleCodeModuleAddress)());
	

	return 0xDEADBEEF;
}