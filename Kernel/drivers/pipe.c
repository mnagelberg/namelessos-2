#include <semaphore.h>
#include <scheduler.h>
//#include <pipe.h>

#define MAX_PIPES 20
#define PIPE_SIZE 200
#define NULL 0	

static struct Pipe * pipes[MAX_PIPES];

struct Pipe {
	Semaphore * sem;
	unsigned size;
	unsigned avail;
	char * buf;
	char * head;
	char * tail;
	char * end;
}; 


int OpenPipe(int fd) {
	if(fd >= MAX_PIPES)	return -1;

	if(pipes[fd] != NULL){
		return fd;
	}

	Page * page = get_page();
	struct Pipe * p = malloc(page, sizeof(struct Pipe));
	p->head = p->tail = p->buf = malloc(page, p->size = PIPE_SIZE);
	p->end = p->buf + PIPE_SIZE;
	p->sem = CreateSem(1);
	pipes[fd] = p;
	return fd;
}

void DeletePipe(int fd) {
	struct Pipe * p = pipes[fd];
	if(p == NULL) return;

	FlushSem(p->sem); // SEM: se asegura que el 
	FlushSemQueue(p->sem); // SEM
	DeleteSem(p->sem); // SEM
	//free(p->sem->page, p);
	pipes[fd] = NULL;
}


/* Si no hay nada que leer, devuelve */
// le diciendo lee del pipe tanto tamaño y metelo en data
int GetPipe(int fd, void * data, unsigned size) {
	struct Pipe * p = pipes[fd];
	if(p == NULL) return -1;
	if(size < 1 || size > p->size) return 0;

	if(!WaitSem(p->sem)) return -1; // setea que nadie me toque ese pipe, te da 0 si alguien lo esta usando

	//read
	int read = 0;
	char * d = data;
	// mientras lo que lei hasta el momento es menor de lo que quiero leer, 
	// head no es tail, o sea que no llega al final
	// y que p->avail WHAT
	while(size > read && p->head != p->tail && p->avail){
		*d++ = *p->head++;
		if(p->head == p->end){
			p->head = p->buf; // p->head lo ubica al principio del pipe
		}
		read--;
		p->avail--;
	}

	SignalSem(p->sem); // libera el semaforo, ya cualquiera puede accederlo

	return read; // retorna cuanto leyo
	// en data esta lo que leiste
}


/*Si no hay lugar para escribir devuelve*/
int PutPipe(int fd, void * data, unsigned size) {
	struct Pipe * p = pipes[fd];
	if(p == NULL) return -1;
	if(size > p->size) return 0;

	if(p->avail < size)	return 0;
		
	char * d = data;
	while(size > 0){
		*p->head++ = *d++;
		if(p->head == p->end){
			p->head = p->buf;
		}
		size--;
		p->avail++;
	}

	SignalSem(p->sem);
	return size; // retorna cuanto le queda 
				 // estoy escriendo la data en el pipe en este metodo
}

int syscall_opipe(int fd) {	
	return OpenPipe(fd);
} 
void syscall_cpipe(int fd) {
	DeletePipe(fd);
}
int syscall_wpipe(int fd, void* data, unsigned int size) {
	return PutPipe(fd, data, size);
}
int syscall_rpipe (int fd, void* data, unsigned int size) {
	return GetPipe(fd, data, size);
}