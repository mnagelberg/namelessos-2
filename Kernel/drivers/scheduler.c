#include <scheduler.h>

uint64_t get_stack_address(void* page_start);
void * fill_stack(void * entry_point, void * user_stack);
Process_Node* new_process_slot(Process* process);
void remove_process(int pid);
void next_process();
static char * state_to_string(int state);



Process_Node * current_node = NULL;

uint64_t sleep_time = 0;
int sleep_pid = 0;
uint8_t sleep_flag = 0;
int foreground_pid = 0;					//PID del shell... cuando esta en 0 todo escribe

/* Change command from kernel back to user*/
void* switch_kernel_to_user() {

	if (current_node ==NULL){
		return 0;
	}

	next_process();
	return current_node->process->user_stack;
}

/* Change command from user to Kernel */
void* switch_user_to_kernel(void * rsp) {


	if (current_node == NULL) {
		return rsp;
	}

	current_node->process->user_stack = rsp;
	return current_node->process->kernel_stack;
}

/* I put a certain process to sleep */
void sleep_scheduler(uint64_t time, int pid) {
	sleep_pid = pid;
	sleep_time = time;
	sleep_flag = 1;
	set_state(pid, SLEEPING);
}

void sleep_process(int pid){
	set_state(pid, SLEEPING);
}

void wake_up_process(int pid){
	set_state(pid, ACTIVE);
}

/* I check if there is any process asleep */
void check_sleeping_scheduler() {
	if (sleep_flag) {
		sleep_time--;

		if (sleep_time == 0) {
			set_state(sleep_pid, ACTIVE);
			sleep_flag = 0;
		}
	}
}

Process * new_process(char * name, void * entry_point, uint8_t foreground) {

	k_mutex(FALSE);

	Process *p = (Process *) get_mem_process(); //el pid ya viene seteado.
	p->user_stack_page = get_page()->page_start;
	p->kernel_stack_page = get_page()->page_start;

	strncpy(name, p->name, 127);

	if (foreground == 1) {
		set_foreground(p->pid);
	}

	if (current_node != NULL) {
		p->ppid = current_node->process->pid;
	}
	else {
		p->ppid = -1;
	}

	p->state = ACTIVE;
	p->entry_point = entry_point;

	p->page = get_page();

	p->user_stack = (void*) get_stack_address(p->user_stack_page);
	p->kernel_stack = (void*) get_stack_address(p->kernel_stack_page);
	p->user_stack = fill_stack(entry_point, p->user_stack);

	k_mutex(TRUE);

	return p;
}

uint64_t get_stack_address(void* page_start) {
	return (uint64_t) ((uint64_t)page_start + PAGE_SIZE - STACK_SIZE - 1);
}

void * fill_stack(void * entry_point, void * user_stack) {
	Process_registers * stack = (Process_registers*) user_stack - 1;
	stack->gs =		0x001;
	stack->fs =		0x002;
	stack->r15 =	0x003;
	stack->r14 =	0x004;
	stack->r13 =	0x005;
	stack->r12 =	0x006;
	stack->r11 =	0x007;
	stack->r10 =	0x008;
	stack->r9 =		0x009;
	stack->r8 =		0x00A;
	stack->rsi =	0x00B;
	stack->rdi =	0x00C;
	stack->rbp =	0x00D;
	stack->rdx =	0x00E;
	stack->rcx =	0x00F;
	stack->rbx =	0x010;
	stack->rax =	0x011;
	stack->rip =	(uint64_t) entry_point;
	stack->cs =		0x008;
	stack->eflags = 0x202;
	stack->rsp =	(uint64_t) &(stack->base);
	stack->ss = 	0x000;
	stack->base =	0x000;

	return stack;
}

Process_Node* new_process_slot(Process* process) {
	
	Process_Node* node = (Process_Node *) get_mem_process_node(process->pid);
	node->process = process;

	return node;
}

/* Add a new process to the scheduling sequence*/
int enqueque_process(Process* p) {
	Process_Node* new_slot = new_process_slot(p);
	if (current_node == NULL) {
		current_node = new_slot;
		current_node->next = current_node;
	}
	else {
		new_slot->next = current_node->next;
		current_node->next = new_slot;
	}

	return current_node->process->pid;
}

void remove_process(int pid) {
	Process_Node* prev_slot = current_node;
	Process_Node* slot_to_remove = current_node->next;

	if (current_node == NULL) {
		return;
	}
	else if (prev_slot == slot_to_remove && prev_slot->process->pid == pid) {
		current_node->process->pid = -1;
		current_node = NULL;
		return;
	}

	while (slot_to_remove->process->pid != pid) {
		prev_slot = slot_to_remove;
		slot_to_remove = slot_to_remove->next;
	}

	slot_to_remove->process->pid = -1;
	prev_slot->next = slot_to_remove->next;
	print_line();
}

void kill(int pid) {
	Process_Node* start = current_node;
	Process_Node* curr = current_node->next;

	if (pid < 0) {
		return;
	}

	remove_process(pid);

	while(start != curr) {
		if (curr->process->ppid == pid) {
			if (get_foreground() == pid) {
				set_foreground(curr->process->ppid);
			}
			curr->process->ppid = 0;
		}
		curr = curr->next;
	}
}

void set_foreground(int pid) {
	//print_string("Seteo foreground: ");
	//print_num_dec(pid);
	//print_line();
	foreground_pid = pid;
}

int get_foreground() {
	return foreground_pid;
}



void set_state(int pid, uint8_t state) {
	Process_Node* start = current_node;
	Process_Node* curr = current_node;

	if (current_node == NULL) {
		return;
	}
	else if (curr == curr->next && curr->process->pid == pid) {
		curr->process->state = state;
		return;
	}

	do {
		curr = curr->next;
	} while (curr != start && curr->process->pid != pid);

	if (curr->process->pid == pid) {
		curr->process->state = state;
	}
}

void next_process() {

	do {
		current_node = current_node->next;
	} while (current_node->process->state == SLEEPING);

}

Process_Node* get_current() {
	return current_node;
}

int get_pid() {
	if (current_node == NULL) {
		return 0;
	}
	return current_node->process->pid;
}

int get_ppid() {
	return current_node->process->ppid;
}

static char * state_to_string(int state) {
	switch (state) {
		case 0:
			return "SLEEPING";

		case 1:
			return "ACTIVE";

		case 2:
			return "PAUSED";

		default:
			return "UNKNOWN";
	}
}

void list_processes() {
	
	Process_Node * this = current_node;

	do
	{
		print_string("PID: ");
		print_num_dec(this->process->pid);
		print_string(" Estado: ");
		print_string(state_to_string(this->process->state));
		print_string(" Nombre: ");
		print_string(this->process->name);
		print_line();
		this=this->next;
	} while (this!=current_node);

}