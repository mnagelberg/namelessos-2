#include <semaphore.h>
#include <scheduler.h>

struct processQueue{
	int pid;
	struct processQueue	* next;	
};


struct Semaphore{
	processQueue * queue;
	int	value;
	processQueue * last;
	Page * page;
};


//CreateSem - aloca un semaforo y establece su cuenta inicial.
Semaphore * CreateSem(unsigned value) {
	Page * page = get_page(); // MUCHACHOS REVISAR ESTO
	Semaphore * sem = malloc(page, sizeof(Semaphore));
	sem->value = value;
	sem->queue = NULL;
	sem->last = NULL;
	return sem;
}


//DeleteSem - da de baja un semaforo.
void DeleteSem(Semaphore *sem){
	FlushSemQueue(sem);
	free(sem->page, sem);
}


int WaitSem(Semaphore *sem) {
	if(sem->value > 0){
		sem->value--;
		return 1;
	}

	struct processQueue* newProc = malloc(sem->page, sizeof(struct processQueue));

	newProc->pid = get_pid();
	newProc->next = NULL;

	if(sem->queue == NULL){

		sem->queue->pid = newProc->pid;
		sem->queue = newProc;
		sem->last = newProc;
	}
	else{
		sem->last->next = newProc;
		sem->last = newProc;
	}

	sleep_process(newProc->pid);
	if(sem->value > 0){
		sem->value--;
		return 1;
	}
	return 0;
}


/*
SignalSem - senaliza un semaforo.
Despierta a la primera tarea de la cola o incrementa la cuenta si la cola
esta vacia.
*/
void SignalSem(Semaphore *sem){
	if ( sem->queue == NULL ){
		sem->value++;
		return;
	}
	int pid = sem->queue->pid;
	sem->queue = sem->queue->next;
	wake_up_process(pid);
}

//ValueSem - informa la cuenta de un semaforo.
unsigned ValueSem(Semaphore *sem) {
	return sem->value;
}

//FlushSem - despierta todas las tareas que esperan en un semaforo.
void FlushSem(Semaphore *sem) {
	sem->value = -1;
	while(sem->queue != NULL){
		SignalSem(sem);
	}
	FlushSemQueue(sem);
}

void FlushSemQueue(Semaphore *sem) {
	struct processQueue * aux;
	while(sem->queue != NULL){
		aux = sem->queue;
		sem->queue = sem->queue->next;
		free(sem->page, aux);
	}
}