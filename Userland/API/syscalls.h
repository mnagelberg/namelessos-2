#ifndef SYSCALLS_HEADER
#define SYSCALLS_HEADER

#include <typeDefs.h>

typedef enum {
READ = 1,
WRITE = 2,
MALLOC = 3,
FREE = 4,
CLS = 5,
EXIT = 6,
SLEEP = 7,
BEEP = 8,
SONG = 9,
LIST_PROCESS = 10,
CREATE_PROCESS = 11,
DRAW_TEST = 12,
KILL_PROCESS = 13
} syscall_id;

int sys_read(FILE_DESC fd, char * string, size_t length);
void sys_write(FILE_DESC fd, const char * string, size_t length);
void sys_beep();
void sys_play_song();
void * sys_malloc(size_t size); //no deberia ser una sys_call, pero para no implementar mmap
void sys_free(void* p);
void sys_cls();
void sys_exit(int exit_code);
void sys_sleep(size_t s);
void sys_list_ps();
void sys_create_process(char * name, void * entry, uint8_t foreground);
void sys_kill_process(int pid);

void sys_draw_test();

#endif
