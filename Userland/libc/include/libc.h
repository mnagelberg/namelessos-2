#ifndef LIBC_HEADER
#define LIBC_HEADER

#include <stdint.h>

int strlen(char * str);
void strcpy(char* dest, const char* src);
int atoi(char* c);
void del_spaces(char* str);
int strcmp(char * str1, char * str2);
void* malloc(int length);
void free(void* p);
void printf(char* fmt, ...);
void putchar(const char c);
char getchar();
int scanf(char* c, int length);
void clear_screen();
void beep();
void play_song();
void exit(int exit_code);

void create_process(char * name, void * entry, uint8_t foreground);

// Nuevas aplicaciones del User Space:
void new_game();
void list_ipcs();
void list_ps();
void kill_process(int pid);

void draw_test();

#endif

