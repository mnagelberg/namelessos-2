#include <ps_comm.h>
//#include <pipe.h>
#include <typeDefs.h>
#include <libc.h>

void open_pipe(){
	//OpenPipe(3);
}

void put_pipe(){
	//PutPipe(3, "Hola!", 6);
}

void get_pipe(){
	//void * data = 
	// malloquear algo de memoria, sizeof "hola!" por ejemplo
	//GetPipe(3, data, 6);
}

void ipcs_test(){
	create_process("Create a pipe", &open_pipe, FALSE);
	create_process("Put in a pipe", &put_pipe, FALSE);
	create_process("Get in a pipe", &get_pipe, FALSE);
}